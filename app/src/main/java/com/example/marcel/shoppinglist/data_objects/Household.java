package com.example.marcel.shoppinglist.data_objects;

import java.util.ArrayList;
import java.util.List;

/**
 * This object is used to store all information about this given household.
 *
 * Created by marcel on 28.03.18.
 */
public class Household {

    private String mId;
    private String mName;
    private List<String> memberList;

    // IDs of the places where items are stored
    private String mArchiveId;
    private String mShoppingListId;
    private String mStockId;

    /**
     * Standard constructor, used if nothing is known about the new household.
     *
     * @param ownerId   the ID of the client who created the household
     * @param name          the name of this household
     */
    public Household (String ownerId, String name) {
        if (ownerId == null || name == null)
            throw new IllegalArgumentException("The owner ID cannot be null");
        mId = "";
        mName = name;
        memberList = new ArrayList<>();
        memberList.add(ownerId);
        mArchiveId = "";
        mShoppingListId = "";
        mStockId = "";
    }

    /**
     * Constructor used the initialise the household with an ID.
     *
     * @param ownerId       the ID of the client who created the household
     * @param name          the name of this household
     * @param householdId   the ID of the household
     */
    public Household (String ownerId, String name, String householdId) {
        this(ownerId, name);

        if (householdId == null)
            throw new IllegalArgumentException("The household ID cannot be null");
        mId = householdId;
    }

    public String getHouseholdId() {
        return mId;
    }

    public String getHouseholdName() {
        return mName;
    }

    public List<String> getMember() {
        return memberList;
    }

    /**
     * Adds the given member ID to the household.
     *
     * @param memberId  the ID of the member which should be added
     */
    public void addMember(String memberId) {
        if ((memberId != null) && (!memberList.contains(memberId)))
            memberList.add(memberId);
    }

    public String getArchiveId() {
        return mArchiveId;
    }

    /**
     * Sets the ID for the archive used by this household. The ID can only be set once.
     *
     * @param archiveId the ID of the archive
     */
    public void setArchiveId(String archiveId) {
        if ((archiveId != null) && (mArchiveId.equals("")))
            mArchiveId = archiveId;
    }

    public String getShoppingListId() {
        return mShoppingListId;
    }

    /**
     * Sets the ID for the shopping list used by this household. The ID can only be set once.
     *
     * @param shoppingListId    the ID of the shopping list
     */
    public void setShoppingListId(String shoppingListId) {
        if ((shoppingListId != null) && (mShoppingListId.equals("")))
            mShoppingListId = shoppingListId;
    }
    public String getStockId() {
        return mStockId;
    }

    /**
     * Sets the ID for the stock used by this household. The ID can only be set once.
     *
     * @param stockId   the ID of the stock
     */
    public void setStockId(String stockId) {
        if ((stockId != null) && (mStockId.equals("")))
            mStockId = stockId;
    }
}
